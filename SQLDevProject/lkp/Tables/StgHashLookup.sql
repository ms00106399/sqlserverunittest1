﻿CREATE TABLE [lkp].[StgHashLookup] (
    [ExecutionId]  BIGINT       NOT NULL,
    [StgTableName] VARCHAR (50) NOT NULL,
    [Hashkey]      VARCHAR (50) NOT NULL,
    [IsDeleted]    BIT          CONSTRAINT [DF_StgHashLookup_IsDeleted] DEFAULT ((0)) NULL,
    [RecordedOn]   DATETIME     NULL,
    CONSTRAINT [PK_StgHashLookup] PRIMARY KEY CLUSTERED ([StgTableName] ASC, [Hashkey] ASC) WITH (FILLFACTOR = 90)
);

