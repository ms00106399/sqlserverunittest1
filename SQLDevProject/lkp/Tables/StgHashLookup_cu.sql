﻿CREATE TABLE [lkp].[StgHashLookup_cu] (
    [ExecutionId]  BIGINT       NOT NULL,
    [StgTableName] VARCHAR (50) NOT NULL,
    [Hashkey]      VARCHAR (50) NOT NULL,
    [IsDeleted]    BIT          NULL,
    [RecordedOn]   DATETIME     NULL,
    CONSTRAINT [PK_StgHashLookup_cu] PRIMARY KEY CLUSTERED ([StgTableName] ASC, [Hashkey] ASC) WITH (FILLFACTOR = 90)
);

