﻿CREATE TABLE [lkp].[StgHashNewInsert] (
    [ExecutionId]  BIGINT       NOT NULL,
    [StgTableName] VARCHAR (50) NOT NULL,
    [Hashkey]      VARCHAR (50) NOT NULL,
    [RecordedOn]   AS           (getdate()),
    CONSTRAINT [PK_StgHashNewInsert] PRIMARY KEY CLUSTERED ([StgTableName] ASC, [Hashkey] ASC) WITH (FILLFACTOR = 90)
);

