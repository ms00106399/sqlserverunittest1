﻿CREATE TABLE [ctl].[Moduledetails] (
    [Moduleid]   INT          NOT NULL,
    [Modulename] VARCHAR (50) NOT NULL,
    [Createdby]  [sysname]    CONSTRAINT [DF__BRSDetail__Creat__2C1E8537] DEFAULT (suser_sname()) NOT NULL,
    [Createdon]  DATETIME     CONSTRAINT [DF__BRSDetail__Creat__2D12A970] DEFAULT (getdate()) NOT NULL,
    [switch]     NCHAR (1)    NULL,
    CONSTRAINT [PK_BRSDetails] PRIMARY KEY CLUSTERED ([Moduleid] ASC) WITH (FILLFACTOR = 90)
);

