﻿CREATE TABLE [ctl].[PackageDetails] (
    [PackageId]   INT           IDENTITY (-1, 1) NOT NULL,
    [PackageName] VARCHAR (100) NOT NULL,
    [PackageType] CHAR (2)      NULL,
    [CreatedBy]   [sysname]     NOT NULL,
    [CreatedOn]   DATETIME      NOT NULL,
    [ModifiedBy]  [sysname]     NULL,
    [ModifiedOn]  DATETIME      NULL,
    CONSTRAINT [PK_IncementalLoad_PackageIncrementalLoadId] PRIMARY KEY CLUSTERED ([PackageId] ASC) WITH (FILLFACTOR = 90)
);

