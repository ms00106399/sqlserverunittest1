﻿CREATE TABLE [ctl].[Modulepackagedetails] (
    [Moduleid]  INT          NOT NULL,
    [Packageid] INT          NOT NULL,
    [Createdon] DATETIME     DEFAULT (getdate()) NOT NULL,
    [Createdby] VARCHAR (50) DEFAULT (suser_sname()) NOT NULL
);

