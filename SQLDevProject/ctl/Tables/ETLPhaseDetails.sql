﻿CREATE TABLE [ctl].[ETLPhaseDetails] (
    [ETLPhaseID]   INT           IDENTITY (1, 1) NOT NULL,
    [ETLPhaseName] VARCHAR (10)  NOT NULL,
    [Description]  VARCHAR (250) NOT NULL,
    [CreatedBy]    [sysname]     DEFAULT (suser_sname()) NOT NULL,
    [CreatedOn]    DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   [sysname]     NULL,
    [ModifiedOn]   DATETIME      NULL,
    CONSTRAINT [PK_ETLID] PRIMARY KEY CLUSTERED ([ETLPhaseID] ASC) WITH (FILLFACTOR = 90)
);

