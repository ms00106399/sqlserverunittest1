﻿CREATE TABLE [ctl].[SourceSystemDetails] (
    [SourceId]   INT          IDENTITY (1, 1) NOT NULL,
    [SourceName] VARCHAR (20) NOT NULL,
    [SourceType] CHAR (2)     NOT NULL,
    [CreatedBy]  [sysname]    NOT NULL,
    [CreatedOn]  DATETIME     NOT NULL,
    [ModifiedBy] [sysname]    NULL,
    [ModifiedOn] DATETIME     NULL,
    CONSTRAINT [PK_SourceSystemDetails] PRIMARY KEY CLUSTERED ([SourceId] ASC) WITH (FILLFACTOR = 90)
);

