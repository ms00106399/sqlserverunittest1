﻿CREATE TABLE [etl].[FileDetails] (
    [FileID]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [FileName]      VARCHAR (100) NOT NULL,
    [FileDate]      VARCHAR (50)  NULL,
    [SeqNo]         VARCHAR (100) NOT NULL,
    [LoadDate]      VARCHAR (50)  NULL,
    [PackageId]     INT           NULL,
    [CreatedOn]     DATETIME      NULL,
    [CreatedBy]     VARCHAR (50)  NULL,
    [BatchId]       INT           NULL,
    [Mode]          NVARCHAR (25) NULL,
    [BatchModeFlag] NCHAR (1)     NULL,
    FOREIGN KEY ([PackageId]) REFERENCES [ctl].[PackageDetails] ([PackageId])
);

