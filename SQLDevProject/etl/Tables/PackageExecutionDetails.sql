﻿CREATE TABLE [etl].[PackageExecutionDetails] (
    [ExecutionId]              BIGINT        IDENTITY (1, 1) NOT NULL,
    [ParentExecutionId]        BIGINT        CONSTRAINT [DF__PackageEx__Paren__689D8392] DEFAULT ((0)) NOT NULL,
    [PackageId]                INT           NOT NULL,
    [PackageStartRun]          DATETIME      CONSTRAINT [DF__PackageEx__Packa__6991A7CB] DEFAULT (getdate()) NOT NULL,
    [PackageFinishedRun]       DATETIME      NULL,
    [PackageStartRunRecord]    VARCHAR (255) NULL,
    [PackageFinishedRunRecord] VARCHAR (255) NULL,
    [ExtractRowCount]          BIGINT        CONSTRAINT [DF__PackageEx__Extra__4242D080] DEFAULT ((0)) NOT NULL,
    [InsertRowCount]           BIGINT        CONSTRAINT [DF__PackageEx__Inser__4336F4B9] DEFAULT ((0)) NOT NULL,
    [UpdateRowCount]           BIGINT        CONSTRAINT [DF__PackageEx__Updat__442B18F2] DEFAULT ((0)) NOT NULL,
    [RejectedRowCount]         BIGINT        CONSTRAINT [DF__PackageEx__Rejec__451F3D2B] DEFAULT ((0)) NOT NULL,
    [ErrRowCount]              BIGINT        CONSTRAINT [DF__PackageEx__ErrRo__46136164] DEFAULT ((0)) NOT NULL,
    [CompletionMessage]        VARCHAR (512) NULL,
    [PackageStatus]            VARCHAR (20)  NULL,
    [InsertedBy]               [sysname]     CONSTRAINT [DF__PackageEx__Inser__6A85CC04] DEFAULT (suser_sname()) NOT NULL,
    [InsertedOn]               DATETIME      NULL,
    [BatchId]                  INT           NULL,
    [Mode]                     NVARCHAR (25) NULL,
    [BatchModeFlag]            NCHAR (1)     NULL,
    CONSTRAINT [PK_AuditId] PRIMARY KEY CLUSTERED ([ExecutionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FKPackageExecutionDetails_PackageDetails] FOREIGN KEY ([PackageId]) REFERENCES [ctl].[PackageDetails] ([PackageId])
);

