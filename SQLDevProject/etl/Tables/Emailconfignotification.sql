﻿CREATE TABLE [etl].[Emailconfignotification] (
    [Config_Key]        INT            IDENTITY (1, 1) NOT NULL,
    [Email_To]          VARCHAR (1000) NULL,
    [Email_From]        VARCHAR (100)  NULL,
    [Email_Cc]          VARCHAR (500)  NULL,
    [Email_Bcc]         VARCHAR (500)  NULL,
    [Email_Subject]     VARCHAR (100)  NULL,
    [Email_Body_Html]   VARCHAR (2000) NULL,
    [Email_Body_Normal] VARCHAR (2000) NULL,
    [Email_Placeholder] VARCHAR (100)  NULL,
    [Email_Purpose]     VARCHAR (100)  NULL,
    [Module_Name]       VARCHAR (100)  NULL,
    [Email_Server]      VARCHAR (100)  NULL,
    [Email_Profile]     VARCHAR (200)  NULL,
    [Created_Date]      DATETIME       CONSTRAINT [DF__EmailConf__Creat__23893F36] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK__EmailCon__A4FE803011D4569C] PRIMARY KEY CLUSTERED ([Config_Key] ASC) WITH (FILLFACTOR = 90)
);

