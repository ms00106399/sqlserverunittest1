﻿CREATE TABLE [etl].[PackageExecutionErr] (
    [ErrId]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [ExecutionId]      BIGINT         NOT NULL,
    [TaskId]           VARCHAR (50)   NULL,
    [TaskName]         VARCHAR (100)  NULL,
    [ErrorCode]        INT            NULL,
    [ErrorColumnName]  VARCHAR (50)   NULL,
    [ErrorDescription] VARCHAR (MAX)  NULL,
    [ErrorData]        VARCHAR (8000) NULL,
    [ErrorType]        VARCHAR (50)   NOT NULL,
    [InsertedBy]       VARCHAR (50)   CONSTRAINT [DF_PackageExecutionErr_InsertedBy] DEFAULT (suser_sname()) NOT NULL,
    [InsertedOn]       DATETIME       CONSTRAINT [DF__PackageEx__Inser__740F363E] DEFAULT (getdate()) NOT NULL,
    [BatchId]          INT            NULL,
    [Mode]             NVARCHAR (25)  NULL,
    [BatchModeFlag]    NCHAR (1)      NULL,
    CONSTRAINT [PK_PackageExecutionErr] PRIMARY KEY CLUSTERED ([ErrId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FKPackageExecutionDetails_PackageExecutionErr] FOREIGN KEY ([ExecutionId]) REFERENCES [etl].[PackageExecutionDetails] ([ExecutionId])
);


GO
ALTER TABLE [etl].[PackageExecutionErr] NOCHECK CONSTRAINT [FKPackageExecutionDetails_PackageExecutionErr];

