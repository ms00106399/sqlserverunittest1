﻿CREATE TABLE [etl].[Emailconfigccforrejection] (
    [Config_Key]    INT            IDENTITY (1, 1) NOT NULL,
    [Email_To]      VARCHAR (1000) NULL,
    [Email_From]    VARCHAR (100)  NULL,
    [Email_Cc]      VARCHAR (500)  NULL,
    [Email_Bcc]     VARCHAR (500)  NULL,
    [Email_Subject] VARCHAR (100)  NULL,
    [Email_Purpose] VARCHAR (100)  NULL,
    [Module_Name]   VARCHAR (100)  NULL,
    [Email_Server]  VARCHAR (100)  NULL,
    [Created_Date]  DATETIME       CONSTRAINT [DF__EmailCCConf__Creat__23893F36] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_EmailCCConfigNotification] PRIMARY KEY CLUSTERED ([Config_Key] ASC) WITH (FILLFACTOR = 80)
);

