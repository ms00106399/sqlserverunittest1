﻿


CREATE PROCEDURE [etl].[USP_UpdateIncrementalLoadValue]
(@SrcLastRefreshedValue varchar(50), @PackageId int)

AS

BEGIN


UPDATE A
SET 
SrcLastRefreshedValue = ISNULL(@SrcLastRefreshedValue,SrcLastRefreshedValue)
From ctl.TableDetails(nolock) A
WHERE PackageId = @PackageId;


END





