﻿

-- ##SUMMARY Procedure to print error message.

CREATE PROCEDURE [etl].[USP_PrintError] (@ErrorNumber int, @ErrorSeverity int, @ErrorState int, @ErrorLine int, @ErrorProcedure nvarchar(200), @ErrorMessage nvarchar(MAX)) AS
Begin
    SET NOCOUNT ON;
	print '@ErrorNumber : ' + cast(@ErrorNumber as VARCHAR(30))
	print '@ErrorSeverity : ' + cast(@ErrorSeverity as VARCHAR(30))
	print '@ErrorState : ' + cast(@ErrorState as VARCHAR(30))
	print '@ErrorLine : ' + cast(@ErrorLine as VARCHAR(30))
	print '@ErrorProcedure : ' + @ErrorProcedure
	print '@ErrorMessage : ' + @ErrorMessage
End




