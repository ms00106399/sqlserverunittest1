﻿

CREATE PROCEDURE [etl].[USP_HashKeyscleanUp]
	-- Add the parameters for the stored procedure here
@SchemaName varchar(50),@Type varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF(@Type='Insert')
BEGIN

IF(@SchemaName='na')
BEGIN

Select 1 as na

END--(@SchemaName='na')


IF(@SchemaName='cu')
BEGIN

INSERT INTO [lkp].[StgHashLookup]
           ([ExecutionId]
           ,[StgTableName]
           ,[Hashkey]
		   ,RecordedOn)
SELECT [ExecutionId]
      ,[StgTableName]
      ,[Hashkey]
	  ,[RecordedOn]
FROM [lkp].[StgHashNewInsert](nolock)
Where ([StgTableName] like 'cu.%')


END
END


IF(@Type='Delete')
BEGIN

IF(@SchemaName='na')
BEGIN

Select 1 as na

END--(@SchemaName='na')

IF(@SchemaName='clr')
BEGIN

Delete
FROM [lkp].[StgHashNewInsert]
Where ([StgTableName] like 'clr.%') --or ([StgTableName] like 'lims.%')

END--(@SchemaName='clr')



END





END




