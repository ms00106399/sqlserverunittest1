﻿  
  
-- ##SUMMARY Procedure for to apply RetentionPolicy on target table based on Retention Period.  
  
CREATE PROCEDURE [etl].[USP_ApplyRetentionPolicy] (@TargetTableName VARCHAR(250), @RetentionPeriod INT, @PackageExecutionId BIGINT, @DebugFlag bit = 0) AS  
  
--Local variable declaration  
DECLARE @InsertedBy AS varchar(50), @PackageStatus AS VARCHAR(MAX), @InsertedOn AS DATETIME, @ErrorType AS VARCHAR(50)  
--declare @TargetSchemaName varchar(30) -- added by anirban  
  
SELECT @InsertedBy = 'USP_ApplyRetentionPolicy', @PackageStatus = 'Failed', @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'    
 
  
BEGIN TRY  
  
 SET NOCOUNT ON  
 --Declare @sp_executesql as Varchar(300), @ParamList Nvarchar(4000), @UpdateQuery nvarchar(MAX)  
 DECLARE @tbl varchar(200) =  @TargetTableName  
  
IF (@RetentionPeriod=0)  
  
  BEGIN  
   DECLARE @dynSQL varchar(500)  
   SET @dynSQL = 'TRUNCATE TABLE ' + @tbl  
  
  END  
  
  IF (@RetentionPeriod>0)  
  
  BEGIN  
  

   SET @dynSQL = N' Delete FROM ' + @tbl + ' WHERE DATEDIFF(dd, RecordedOn, GETDATE()) >=' + Cast(@RetentionPeriod as varchar(10))  
  
  END  
  
EXEC (@dynSQL)  
  
 Print ''  
 Print 'USP_ApplyRetentionPolicy Executed Successfully...'  
 Print ''  
  
END TRY  
  
BEGIN CATCH  
 DECLARE @ErrorMessage nvarchar(MAX), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;  
 SELECT @ErrorProcedure = N'USP_ApplyRetentionPolicy', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);  
 SELECT @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + cast(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' +
 ERROR_MESSAGE();  
END CATCH  

