﻿


-- ##SUMMARY Procedure to perform pre-execution activities.

CREATE PROCEDURE [etl].[USP_PreExecution]
(@ParentExecutionId BIGINT, @PackageId int, @PackageStartRunRecord varchar(50), @ExecutionId BIGINT OUTPUT, @DebugFlag bit = 0)

AS


--Local variable declaration
DECLARE @InsertedBy AS varchar(50), @PackageStatus AS VARCHAR(MAX), @InsertedOn AS DATETIME, @ErrorType AS VARCHAR(50)

SELECT @InsertedBy = 'USP_PreExecution', @PackageStatus = 'Failed', @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'

BEGIN TRY

	SET NOCOUNT ON

	If @DebugFlag = 1 
	Begin
		print ''
		print 'Inside USP_PreExecution : '
		print '--------------------------'		
	End

--Update Prv Date
UPDATE A
SET 
SrcPrvRefreshedValue = SrcLastRefreshedValue
From ctl.TableDetails(nolock) A
WHERE PackageId = @PackageId;



INSERT INTO etl.PackageExecutionDetails 
 (ParentExecutionId, PackageId, PackageStartRun, PackageStartRunRecord, PackageStatus, InsertedOn) 
VALUES (@ParentExecutionId, @PackageId, GETDATE(), @PackageStartRunRecord, 'Started', GetDate());

SELECT @ExecutionId = SCOPE_IDENTITY();



	Print ''
	Print 'USP_PreExecution Executed Successfully...'
	Print ''

END TRY

BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(MAX), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
	SELECT @ErrorProcedure = N'USP_PreExecution', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
	SELECT @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + cast(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' + ERROR_MESSAGE();
	If @DebugFlag = 1 Exec etl.USP_PrintError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity,  @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage

INSERT INTO [etl].[PackageExecutionErr] (ExecutionId, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
VALUES (@ExecutionId, CAST(@ErrorNumber AS BIGINT), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)

	--EXEC USP_LogError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity, @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage, @DebugFlag = @DebugFlag;
END CATCH






