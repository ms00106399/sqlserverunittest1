﻿


-- ##SUMMARY Procedure to delete invalid or failed lookup values.

CREATE PROCEDURE [etl].[USP_GetLastRefreshDate] (@TableName varchar(100), @ErrorCount bigInt, @LastRefreshDate varchar(50), @DebugFlag BIT = 0) --, @NewRefreshDate varchar(50) output

AS

BEGIN TRY

	SET NOCOUNT ON

	--print 'Inside Proc'
	--print '@ErrorCount' + cast(@ErrorCount as varchar(30))
	--print '@LastRefreshDate: ' + @LastRefreshDate
	--print '@TableName: ' + @TableName


  Declare @SQL nvarchar(500)

  SELECT @SQL = 'Select Case When ' + Cast(@ErrorCount as varchar(10)) + '> 0 Then ''' + @LastRefreshDate + ''' Else Max(CONVERT(VARCHAR(30), ISNULL(RecordedOn,''1900-01-01''), 121)) END As LastRefreshedValue from '+ @TableName + '(nolock)'

IF @DebugFlag = 1
BEGIN 
 print '@SQL: ' + ISNULL(@SQL, 'NULL')
END 
EXEC sp_executesql @SQL

 --Return @SetLastRefreshDate

  --Select Max(CONVERT(CHAR(30), RecordedOn, 121)) As LastRefreshedValue From clr.CoefficientTags_V



END TRY

BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(MAX), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
	SELECT @ErrorProcedure = N'USP_CleanupHashLookupData', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
	SELECT @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + cast(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' +
 ERROR_MESSAGE();
	--If @DebugFlag = 1 Exec etl.USP_PrintError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity,  @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage

--INSERT INTO [etl].[PackageExecutionErr] (ExecutionId, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
--VALUES (@PackageExecutionId, CAST(@ErrorNumber AS BIGINT), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)

	--EXEC USP_LogError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity, @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage, @DebugFlag = @DebugFlag;
END CATCH






