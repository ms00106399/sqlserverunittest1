﻿



-- ##SUMMARY Procedure to capture error details when any failure while executing package(s).

CREATE PROCEDURE [etl].[USP_GetPackageDetails]
(@PackageName varchar(250), @DebugFlag bit = 0)

As

	SET NOCOUNT ON

	If @DebugFlag = 1 
	Begin
		print ''
		print 'Inside USP_GetPackageDetails : '
		print '---------------------------------'		
	End

Declare @count int=0

Select @count=Count(1) from ctl.PackageDetails(noLock)
Where PackageName=@PackageName

IF @DebugFlag = 1
BEGIN
	PRINT '@Count : ' + CAST(@Count AS VARCHAR(10))
END

IF (@count=0)
BEGIN

Select PackageId, SourceId, ETLPhaseID, SrcLastRefreshedValue, '' SrcTableName, '1' As SrcKeyColumnName, 'Varchar(20)' SrcKeyColumnDataType, 'NA' SourceName, (TargetSchemaName +'.'+TargetTableName) TargetTableName, RetentionPeriod
From ctl.TableDetails(noLock) WHERE TableId=-1
END

Else
 Begin 

Select ISNULL(TD.PackageId,-1) PackageId, ISNULL(TD.SourceId,-1) SourceId, ISNULL(TD.ETLPhaseID,-1) ETLPhaseID, 
          ISNULL(TD.SrcLastRefreshedValue,'NA') SrcLastRefreshedValue, CASE When SrcSchemaName='NA' or SrcSchemaName='' THEN SrcTableName ELSE (SrcSchemaName + '.' + SrcTableName) END SrcTableName, 
		 -- (SrcTableName) as SrcTableName,
		  ISNULL(TD.SrcKeyColumnName,'NA') SrcKeyColumnName, 
		      Case When ISNULL(TD.SrcKeyColumnDataType,'NA') ='NA' Then 'Varchar(20)' Else TD.SrcKeyColumnDataType END As SrcKeyColumnDataType, 
			  SSD.SourceName, (TargetSchemaName +'.'+TD.TargetTableName) TargetTableName, TD.RetentionPeriod
		From ctl.TableDetails(noLock) TD
 RIGHT OUTER JOIN ctl.PackageDetails(noLock) PD ON ISNULL(TD.PackageId,-1) = ISNULL(PD.PackageId,-1)
 RIGHT OUTER JOIN ctl.SourceSystemDetails(noLock) SSD ON ISNULL(TD.SourceId,-1) = ISNULL(SSD.SourceId,-1)
   WHERE PD.PackageName = @PackageName

END


	Print ''
	Print 'USP_GetPackageDetails Executed Successfully...'
	Print ''



