﻿

-- ##SUMMARY Procedure to capture error details when any failure while executing package(s).

CREATE PROCEDURE [etl].[USP_CaptureErrorDetails] (@PackageExecutionId BIGINT, @TaskId VARCHAR(50), @TaskName VARCHAR(100), @ErrorCode BIGINT, @ErrorDescription NVARCHAR(MAX), @DebugFlag bit = 0) AS

--Local variable declaration
DECLARE @InsertedBy AS varchar(50), @PackageStatus AS VARCHAR(MAX), @InsertedOn AS DATETIME, @ErrorType AS VARCHAR(50)

SELECT @InsertedBy = 'USP_CaptureErrorDetails', @PackageStatus = 'Failed', @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'

BEGIN TRY

	SET NOCOUNT ON

	If @DebugFlag = 1 
	Begin
		print ''
		print 'Inside USP_CaptureErrorDetails : '
		print '---------------------------------'		
	End

--Updating ErrorLogs table with error details
INSERT INTO [etl].[PackageExecutionErr] (ExecutionId, TaskId, TaskName, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
VALUES (@PackageExecutionId, @TaskId, @TaskName, @ErrorCode, @ErrorDescription, @ErrorType, @InsertedBy, @InsertedOn)

--Updating ExtractLogs table with error details
UPDATE etl.PackageExecutionDetails SET PackageStatus = @PackageStatus, PackageFinishedRun = @InsertedOn
, InsertedBy = @InsertedBy
, InsertedOn = @InsertedOn
 WHERE ExecutionId = @PackageExecutionId


	Print ''
	Print 'USP_CaptureErrorDetails Executed Successfully...'
	Print ''

END TRY

BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(MAX), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
	SELECT @ErrorProcedure = N'USP_CaptureErrorDetails', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
	SELECT @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + cast(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' + ERROR_MESSAGE();
	If @DebugFlag = 1 Exec etl.USP_PrintError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity,  @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage

INSERT INTO [etl].[PackageExecutionErr] (ExecutionId, TaskId, TaskName, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
VALUES (@PackageExecutionId, @TaskId, @TaskName, CAST(@ErrorNumber AS BIGINT), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)

	--EXEC USP_LogError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity, @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage, @DebugFlag = @DebugFlag;
END CATCH



