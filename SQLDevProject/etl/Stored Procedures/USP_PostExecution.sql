﻿

-- ##SUMMARY Procedure to perform post execution activities.

CREATE PROCEDURE [etl].[USP_PostExecution] (@RecordInsert Bigint, @RecordErr Bigint, @RecordUpdate Bigint, @PackageFinishedRunRecord varchar(255), @ExecutionId Bigint, @RecordExtract Bigint, @RecordRejected BIGINT, @DebugFlag bit = 0) AS

--Local variable declaration
DECLARE @InsertedBy AS varchar(50), @PackageStatus AS VARCHAR(MAX), @InsertedOn AS DATETIME, @ErrorType AS VARCHAR(50)

SELECT @InsertedBy = 'USP_PostExecution', @PackageStatus = 'Failed', @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'

BEGIN TRY

	SET NOCOUNT ON

	If @DebugFlag = 1 
	Begin
		print ''
		print 'Inside USP_PostExecution : '
		print '---------------------------'		
	End

Declare @CompletionMessage varchar(250)


Set @RecordInsert=(Select Case when @RecordInsert<0 Then 0 Else @RecordInsert End)

SET @PackageStatus=(
                         Select Case When @RecordInsert>=0 and @RecordErr=0 Then 'Success'
                                     When @RecordInsert>0 and @RecordErr>0 Then 'PartialSuccess'
          When @RecordInsert=0 and @RecordErr>0 Then 'Failure' ELSE 'Failure'
          END As PackageStatus
                         )
						 print @PackageStatus

SET @CompletionMessage=(
                         Select Case When @RecordInsert>=0 and @RecordErr=0 Then 'Process Succeeded - ' + Cast(@RecordInsert as Varchar(100)) + ' rows processed.'
                                     When @RecordInsert>0 and @RecordErr>0 Then 'Process Partial Succeeed - ' + Cast(@RecordInsert as Varchar(100)) + ' rows processed and ' + Cast(@RecordErr as Varchar(100)) + ' err rows processed.'
          When @RecordInsert=0 and @RecordErr>0 Then 'Process Failed - ' + Cast(@RecordErr as Varchar(100)) + ' err rows processed.'
          END As CompletionMessage
                         )

UPDATE A
SET
PackageFinishedRun = GETDATE(),
PackageFinishedRunRecord = @PackageFinishedRunRecord,
ExtractRowCount = @RecordExtract,
InsertRowCount= @RecordInsert,
UpdateRowCount = @RecordUpdate,
RejectedRowCount = @RecordRejected,
ErrRowCount = @RecordErr,
CompletionMessage = @CompletionMessage,
PackageStatus = @PackageStatus
From etl.PackageExecutionDetails(Nolock) A 
WHERE ExecutionId = @ExecutionId



	Print ''
	Print 'USP_PostExecution Executed Successfully...'
	Print ''

END TRY

BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(MAX), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
	SELECT @ErrorProcedure = N'USP_PostExecution', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
	SELECT @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + cast(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' + ERROR_MESSAGE();
	If @DebugFlag = 1 Exec etl.USP_PrintError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity,  @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage

INSERT INTO [etl].[PackageExecutionErr] (ExecutionId, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
VALUES (@ExecutionId, CAST(@ErrorNumber AS BIGINT), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)

	--EXEC USP_LogError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity, @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage, @DebugFlag = @DebugFlag;
END CATCH





