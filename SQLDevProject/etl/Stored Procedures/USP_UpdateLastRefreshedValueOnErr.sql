﻿



CREATE PROCEDURE [etl].[USP_UpdateLastRefreshedValueOnErr] 
(@ExecutionId Bigint, @DebugFlag bit = 0) 
AS

--Local variable declaration
DECLARE @InsertedBy AS varchar(50), @PackageStatus AS VARCHAR(MAX), @InsertedOn AS DATETIME, @ErrorType AS VARCHAR(50)

SELECT @InsertedBy = 'USP_PostExecution', @PackageStatus = 'Failed', @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'

BEGIN TRY

	SET NOCOUNT ON

	If @DebugFlag = 1 
	Begin
		print ''
		print 'Inside USP_UpdateLastRefreshedValueOnErr : '
		print '---------------------------'		
	End

Declare @CompletionMessage varchar(250);
Declare @PackageName varchar(250);
DECLARE @ETLPhaseId int;

Set @PackageName = (
Select [PackageName] From [ctl].[PackageDetails](nolock)
  Where [PackageId]=(Select Distinct [PackageId] from [etl].[PackageExecutionDetails](nolock)
where [ExecutionId]=@ExecutionId)
)

SET @ETLPhaseId = (Select Case When @PackageName Like 'STG%' Then 1 Else 2 END As ETLPhaseId)

IF (@ETLPhaseId=1)

BEGIN

With CTE
AS
(
Select Distinct [PackageId] from [etl].[PackageExecutionDetails](nolock)
where [ParentExecutionId]=@ExecutionId
)--With CTE

Update [ctl].[TableDetails]
SET [SrcLastRefreshedValue]=[SrcPrvRefreshedValue]
Where [PackageId] in(Select PackageId from CTE);

END--IF (@ETLPhaseId=1)


Else--(@ETLPhaseId=2)

BEGIN

SET @PackageName = (Select 'STG_' + Substring(@PackageName,4,Len(@PackageName)))

;With CTE
AS
(
Select Distinct [PackageId] from [etl].[PackageExecutionDetails](nolock)
where [ParentExecutionId]=
(
Select Max(ExecutionId)As ExecutionId from [etl].[PackageExecutionDetails](nolock)
Where [PackageId]=(Select [PackageId] from [ctl].[PackageDetails](nolock) Where [PackageName]=@PackageName)
)
)--With CTE

Update [ctl].[TableDetails]
SET [SrcLastRefreshedValue]=[SrcPrvRefreshedValue]
Where [PackageId] in(Select PackageId from CTE);

END--Else--(@ETLPhaseId=2)


	Print ''
	Print 'USP_UpdateLastRefreshedValueOnErr Executed Successfully...'
	Print ''

END TRY

BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(MAX), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
	SELECT @ErrorProcedure = N'USP_PostExecution', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
	SELECT @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + cast(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' + ERROR_MESSAGE();
	If @DebugFlag = 1 Exec etl.USP_PrintError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity,  @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage

INSERT INTO [etl].[PackageExecutionErr] (ExecutionId, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
VALUES (@ExecutionId, CAST(@ErrorNumber AS BIGINT), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)

	--EXEC USP_LogError @ErrorNumber = @ErrorNumber, @ErrorSeverity = @ErrorSeverity, @ErrorState = @ErrorState, @ErrorLine = @ErrorLine, @ErrorProcedure = @ErrorProcedure, @ErrorMessage = @ErrorMessage, @DebugFlag = @DebugFlag;
END CATCH



