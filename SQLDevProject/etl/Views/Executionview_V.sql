﻿

CREATE View [etl].[Executionview_V]

As

SELECT A.[ExecutionId]
      ,[ParentExecutionId]
	  ,A.PackageId
      ,[PackageName]
      ,[PackageStartRun]
      ,[PackageFinishedRun]
      ,[PackageStartRunRecord]
      ,[PackageFinishedRunRecord]
	  ,SourceName
	  ,[SourceType]
	  ,([SrcSchemaName]+'.'+[SrcTableName]) As SrcTableName
	  ,([TargetSchemaName]+'.'+[TargetTableName]) As TargetTableName
      ,[ExtractRowCount]
      ,[InsertRowCount]
      ,[UpdateRowCount]
      ,[RejectedRowCount]
      ,[ErrRowCount]
      ,[CompletionMessage]
      ,[PackageStatus]
      ,[InsertedBy]
      ,[InsertedOn]
  FROM [etl].[PackageExecutionDetails](nolock) A
  Inner Join [ctl].[PackageDetails](nolock) B  ON A.PackageId=B.PackageId
  INNER JOIN [ctl].[TableDetails] D ON A.PackageId=D.PackageId
  INNER JOIN [ctl].[SourceSystemDetails] E ON D.SourceId=E.SourceId
  --Order by 1 desc


