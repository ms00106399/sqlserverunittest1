﻿CREATE View [etl].[Executionerrview_V]

As

SELECT A.[ExecutionId]
      ,[ParentExecutionId]
	  ,[ETLPhaseID]
	  ,A.PackageId
      ,[PackageName]
      ,[PackageStartRun]
      ,[PackageFinishedRun]
	  ,SourceName
	  ,[SourceType]
	  ,([SrcSchemaName]+'.'+[SrcTableName]) As SrcTableName
	  ,([TargetSchemaName]+'.'+[TargetTableName]) As TargetTableName
      ,TaskName
      ,ErrorCode
      ,ErrorColumnName
      ,ErrorDescription
      ,ErrorData
      ,ErrorType
      ,C.[InsertedBy]
      ,C.[InsertedOn]
  FROM [etl].[PackageExecutionDetails](nolock) A
  INNER JOIN [ctl].[PackageDetails](nolock) B ON A.PackageId=B.PackageId
  INNER JOIN [etl].[PackageExecutionErr](nolock) C  ON A.ExecutionId=C.ExecutionId
  INNER JOIN [ctl].[TableDetails] D ON A.PackageId=D.PackageId
  INNER JOIN [ctl].[SourceSystemDetails] E ON D.SourceId=E.SourceId
  --Order by 1 desc

