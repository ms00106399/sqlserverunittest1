﻿CREATE TABLE [config].[EnvironmentParameters] (
    [ParameterID]          INT             IDENTITY (1, 1) NOT NULL,
    [ParameterName]        NVARCHAR (50)   NOT NULL,
    [ParameterDescription] VARCHAR (1000)  NULL,
    [ParameterValue]       NVARCHAR (1000) NOT NULL,
    [CreatedDate]          DATETIME        NULL,
    [LastUpdatedDate]      DATETIME        NULL,
    [LastUpdatedBy]        NVARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([ParameterID] ASC) WITH (FILLFACTOR = 90)
);

