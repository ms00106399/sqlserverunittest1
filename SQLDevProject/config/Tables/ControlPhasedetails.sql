﻿CREATE TABLE [config].[ControlPhasedetails] (
    [Phaseid]   INT          NOT NULL,
    [Phasename] VARCHAR (50) NOT NULL,
    [Createdby] [sysname]    NOT NULL,
    [Createdon] DATETIME     NOT NULL,
    [switch]    NCHAR (1)    NULL,
    PRIMARY KEY CLUSTERED ([Phaseid] ASC) WITH (FILLFACTOR = 90)
);

