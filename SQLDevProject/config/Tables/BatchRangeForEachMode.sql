﻿CREATE TABLE [config].[BatchRangeForEachMode] (
    [Mode]        NVARCHAR (25) NULL,
    [LowerValue]  INT           NULL,
    [HigherValue] INT           NULL,
    [MaxBatchID]  INT           NULL,
    [LastUpdated] DATETIME      NULL
);

