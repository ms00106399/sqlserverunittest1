﻿CREATE TABLE [config].[Lkp_AccessTechProductVariant] (
    [ProductNumber]    NVARCHAR (20)  NULL,
    [ProductVariant]   NVARCHAR (100) NULL,
    [AccessTechnology] NVARCHAR (20)  NULL
);

