﻿CREATE TABLE [config].[ConfigParamValParentPkgExec] (
    [SSISCatalogFolderName]    NVARCHAR (100) NULL,
    [SSISCatalogProjectName]   NVARCHAR (100) NULL,
    [DRA_InitialCatalog]       NVARCHAR (100) NULL,
    [DRA_ServerName]           NVARCHAR (100) NULL,
    [PME_InitialCatalog]       NVARCHAR (100) NULL,
    [PME_ServerName]           NVARCHAR (100) NULL,
    [CRM_InitialCatalog]       NVARCHAR (100) NULL,
    [CRM_ServerName]           NVARCHAR (100) NULL,
    [FlatFileConn_SharedDrive] NVARCHAR (100) NULL,
    [FlatFileArchiveFolder]    NVARCHAR (100) NULL,
    [FlatFileName]             NVARCHAR (100) NULL,
    [FlatFileRejectFolder]     NVARCHAR (100) NULL,
    [FlateFileFolder]          NVARCHAR (100) NULL,
    [ParentPackage]            NVARCHAR (100) NULL,
    [ADMAN_ConnectionName]     NVARCHAR (100) NULL,
    [ADMAN_Pwd]                NVARCHAR (100) NULL,
    [ADMAN_User]               NVARCHAR (100) NULL,
    [INCA_ConnectionName]      NVARCHAR (100) NULL,
    [INCA_Pwd]                 NVARCHAR (100) NULL,
    [INCA_User]                NVARCHAR (100) NULL
);

