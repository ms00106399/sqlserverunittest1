﻿CREATE TABLE [config].[PackageExecutionStatusCheck] (
    [Mode]                   NVARCHAR (MAX) NULL,
    [PackageName]            NVARCHAR (MAX) NULL,
    [ExecutionId]            INT            NULL,
    [ExecutionStatus]        INT            NULL,
    [ExecutionStartDateTime] DATETIME       NULL,
    [ExecutionEndDateTime]   DATETIME       NULL
);

