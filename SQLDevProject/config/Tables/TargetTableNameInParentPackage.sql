﻿CREATE TABLE [config].[TargetTableNameInParentPackage] (
    [ParentPackageName] NVARCHAR (100) NULL,
    [SchemaName]        NVARCHAR (10)  NULL,
    [TargetTable]       NVARCHAR (100) NULL
);

