﻿CREATE TABLE [config].[MigrationCandidateManager] (
    [Mode]             NVARCHAR (25)  NULL,
    [BatchID]          INT            NULL,
    [SPCustomerId]     NVARCHAR (10)  NULL,
    [SPAccountId]      NVARCHAR (10)  NULL,
    [LID]              NVARCHAR (8)   NULL,
    [AccessTechnology] NVARCHAR (25)  NULL,
    [ProductVariant]   NVARCHAR (100) NULL,
    [Failure_Flag]     NCHAR (1)      CONSTRAINT [DF_MigrationCandidateManager_Failure_Flag] DEFAULT ('N') NULL,
    [Status]           NVARCHAR (25)  NULL,
    [RecordedOn]       DATETIME       NULL,
    [LastUpdated]      DATETIME       NULL
);

