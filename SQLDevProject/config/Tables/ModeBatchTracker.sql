﻿CREATE TABLE [config].[ModeBatchTracker] (
    [Mode]        NVARCHAR (25) NULL,
    [BatchID]     INT           NULL,
    [LastUpdated] DATETIME      CONSTRAINT [DF_ModeBatchTracker_LastUpdated] DEFAULT (getdate()) NULL
);

