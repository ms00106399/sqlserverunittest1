﻿CREATE TABLE [config].[MigrationSourceControl] (
    [Mode]                         NVARCHAR (25)  NULL,
    [Source]                       NVARCHAR (30)  NULL,
    [Source_Package]               NVARCHAR (100) NULL,
    [Switch]                       NCHAR (1)      NULL,
    [Last_Successful_Completion]   DATETIME       NULL,
    [Last_Successful_DAN_Batch_Id] INT            NULL
);

