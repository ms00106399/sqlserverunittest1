﻿

CREATE PROCEDURE [config].[USP_UpdateConfigAndFileTableWithModeAndBatch]
@StartDatetime   datetime,
@EndDatetime datetime,
@ExecutionId1	bigint=0
as
 begin
 	Declare @InsertedBy as nvarchar(50), @InsertedOn as datetime, @ErrorType as nvarchar(50)
	Select @InsertedBy = 'USP_UpdateConfigAndFileTableWithModeAndBatch',  @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'
	Begin Try
	Set nocount on
 					Update etl.Filedetails 
					set mode		  = (select mode from config.ModeBatchTracker(nolock)), 
						BatchID		  = (select BatchID from config.ModeBatchTracker(nolock)), 
						BatchModeFlag = 'Y'
					where Loaddate between @StartDatetime and @EndDatetime and BatchModeFlag = 'N'

					Update etl.Packageexecutiondetails 
					set mode		  = (select mode from config.ModeBatchTracker(nolock)), 
						BatchID		  = (select BatchID from config.ModeBatchTracker(nolock)), 
						BatchModeFlag = 'Y'
					where Packagefinishedrun between @StartDatetime and @EndDatetime and BatchModeFlag = 'N'

					Update etl.Packageexecutionerr
					set mode		  = (select mode from config.ModeBatchTracker(nolock)), 
						BatchID		  = (select BatchID from config.ModeBatchTracker(nolock)), 
						BatchModeFlag = 'Y'
					where Insertedon  between @StartDatetime and @EndDatetime and BatchModeFlag = 'N'
	End Try
	Begin Catch
			Declare @ErrorMessage nvarchar(max), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
			Select @ErrorProcedure = N'USP_UpdateConfigAndFileTableWithModeAndBatch', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
			Select @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + CAST(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' + ERROR_MESSAGE();

			insert into [etl].[PackageExecutionErr] (ExecutionId, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
			values (@ExecutionId1, CAST(@ErrorNumber as bigint), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)
	End Catch
 end
