﻿
  
  
 
--	----select    * from config.BatchRangeForEachMode(nolock) where mode = @mode 
CREATE Procedure [config].[USP_GetDRABatcheValue]( @mode nvarchar(25), @ExecutionId1 bigint=0 )
as
begin
 	Declare @InsertedBy as nvarchar(50), @InsertedOn as datetime, @ErrorType as nvarchar(50)
	Select @InsertedBy = 'USP_GetDRABatcheValue',  @InsertedOn = GETDATE(), @ErrorType = 'Runtime Error'
	Begin Try
	Set nocount on
 
	declare @batchID int 
	declare @maxBatchId int 
	declare @maxBatchIdFirstTime int
	declare @lowerValue int
	declare @higherValue int
	set		@batchID=''
	--set @mode = 'DAN'

	select     @maxBatchIdFirstTime = Isnull(MaxBatchID,LowerValue), @maxBatchId =  MaxBatchID   from config.BatchRangeForEachMode(nolock) 	
	where  mode = @mode and Isnull(MaxBatchID,LowerValue) between LowerValue and HigherValue

		   if @mode ='DAN'
				begin
					set @batchID = @maxBatchIdFirstTime
				end
			else
				begin
				   set @batchID = case when @maxBatchId is null then @maxBatchIdFirstTime else @maxBatchId + 1 end
					--set @batchID = @maxBatchId +1
				end
		--end
		select @batchID as batchID
		End Try
	Begin Catch
			Declare @ErrorMessage nvarchar(max), @ErrorNumber int, @ErrorSeverity int, @ErrorProcedure nvarchar(200), @ErrorState int, @ErrorLine int;
			Select @ErrorProcedure = N'USP_GetDRABatcheValue', @ErrorNumber = ISNULL(ERROR_NUMBER(), 0), @ErrorSeverity = ISNULL(ERROR_SEVERITY(), 0), @ErrorState = ISNULL(ERROR_STATE(), 0), @ErrorLine = ISNULL(ERROR_LINE(), 0);
			Select @ErrorMessage = N'[' + @ErrorProcedure + '(ErrorNo:' + CAST(@ErrorNumber as varchar(30)) + ', Severity:' + cast(@ErrorSeverity as varchar(30)) + ', State:' + cast(@ErrorState as varchar(30)) + ', Line:' + cast(@ErrorLine as varchar(30)) + ')]: ' + ERROR_MESSAGE();

			insert into [etl].[PackageExecutionErr] (ExecutionId, ErrorCode, ErrorDescription, ErrorType, InsertedBy, InsertedOn) 
			values (@ExecutionId1, CAST(@ErrorNumber as bigint), @ErrorMessage, @ErrorType, @InsertedBy, @InsertedOn)
	End Catch
end
